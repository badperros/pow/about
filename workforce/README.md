# Workforce

Actualmente estamos laburando en cuarentena de acuerdo a [P.A.C.O. (Plan de Acción de Cuarentena y Oficina)](/workforce/paco)

Si queres laburar con nosotros, [entra acá](/workforce/join-us).

## Comunicación

¿Dónde están las tareas? ¿Cómo me comunico con un coworker? [Click acá](/workforce/communication)

## Valores de la empresa

Es importante definir un conjunto de valores que nos representen, estos los vas a encontrar [acá](/workforce/manifesto)

## Workflow

[Cómo trabajamos?](./workflow)
# Workflow

* Cómo es el proceso de trabajo?
* Cómo se pasa de los requisitos del cliente a una planificación con estimados?
* Cómo es el proceso de control de calidad?

Bueno, vamos a ir por pasos, pero primero repasemos un par de conceptos:

## Enfoque

En POW nos concentramos en las personas que trabajan, no en los clientes ni en los proyectos, creemos que una cultura sana de trabajo es primordial, y todos estos procesos de trabajo están orientados a hacer que trabajar con nosotros sea lo mas ameno posible. Sabemos que esto, para nosotros, no "rinde" tanto desde el punto de vista comercial, pero nuestras ambiciones van mucho más allá de lo económico.

## Comunicación

Esto es tan importante que le dedicamos una sección entera, que podes chequear [acá](/workforce/communication)

## Roles en POW

La estructura de POW es algo así:

![roles](./assets/roles.jpg)

Cada departamento de lo que hacemos en POW tiene un lead / responsable y nuestro nexo con el cliente siempre es por medio del Project Manager / Product Owner.

### **Observaciones**

**No es más fácil permitir que el cliente hable directamente con el equipo?**  
No, creeme que no. La comunicación con el cliente es un arte en sí, y tiene sus cosas particulares:
* Qué pasa si el cliente está en otra zona horaria? Vas a contestar mails a las 4 AM?
* Se retrasaron las fechas del proyecto y el cliente se enojó, de verdad querés leer las cosas que te pueden llegar a decir?
* El cliente tiene una idea que nos cambia todo el proyecto, no es mejor dejar que alguien más lo baje a la realidad explicándole por qué no es viable?

# Paso a paso del proyecto

Esto va a ser una especie de ["Elige tu propia aventura"](https://es.wikipedia.org/wiki/Elige_tu_propia_aventura), agarrate.

## La mañana de Robin, Project Manager:

Era una tibia mañana de primavera, los pájaros cantan, el portero del edificio riega la vereda, y una persona está insultando a otra porque le estacionó en la bajada del garage. Como todas las mañanas cuando te despertás, pasas unas 3 horas más en la cama hasta que juntas ganas de levantarte, te duchas, desayunas y arrancas tu día.

En qué consiste tu día? Bueno eso lo sabrás vos, pero probablemente revisás tu mail y encontrás 16 newsletters, el resumen de la tarjeta que llegó vencido porque ningún homebanking funciona bien y oh! que sorpresa! 3 mails que no sabés de qué tratan.  

Vamos con el primero, que es un mail de Andra, qué querrá no? Si no se ven desde salita de 3 del jardín. Abrís el mail y te encontrás con esto:

> Hola Robin!  
Cómo andas tanto tiempo? Recuerdo cuando comíamos Trakinas en salita de 3, probablemente no te acordás de mi, pero mi vieja me comentó que eras hacker y hacías apps.
Te cuento un poco de lo que estuve haciendo, viste que mi familia se dedica a todo lo que es plástico? Bueno, hace unos años abrí una fábrica y produzco las tapitas de las lapiceras.  
Qué tipo de lapiceras te estarás preguntando, cualquier tipo de lapiceras. Las hacemos personalizadas, suponete que queres tapitas de lapiceras y que en la punta tenga un busto de San Martín, bueno a nosotros nos mandas el modelo y lo hacemos, o elegís de los que tenemos en stock (recién ingresaron unos con forma de escarbadiente, son re útiles.).  
Necesitaría una página donde pueda mostrar los diseños que ya tengo y una manera de que me lleguen los trabajos a medida que la gente quiera, también tengo un Instagram que no le doy mucha bola, pero me llegan mensajes de vez en cuando. Y cómo hago para aparecer en Google? Me gustaría darle un poco mas de exposición a mi emprendimiento y me vendría bien un poco de asesoramiento.  
Gracias por leerme, espero que estés bien!

|¿Qué haces?|
|:--:|
|[Me interesa y quiero darle una mano.](./story/0)|
|[Me acuerdo que Andra me mordía en salita de 3 y le ignoro.](./story/1)|
# Me interesa y quiero darle una mano.

Tenés pequeños recuerdos de esa persona, pero ves una oportunidad de traer un proyecto a la empresa y te pones a trabajar.

> Hola Andra!  
Si! En mi empresa hacemos sitios web y nos encargamos de marketing digital. Si querés podemos juntarnos o hacer una videollamada y me contás un poco más de lo que necesitas.

Después de bastantes mails de idas y vueltas, una videollamada y bastantes problemas de interpretación, crees que llegaste a un documento conteniendo todas las cosas que considerás necesarias. [El documento al que llegas es este](/workforce/workflow/flows/website/docs/requirements.md).

Con ese documento armado, ya podés sumarlo a ClickUp en Incoming; y podés ir a hablar con las cabezas de los departamentos para obtener estimados. Así que entras al Discord y avisas que llegó un posible nuevo proyecto (link de ClickUp) y necesitas ayuda para cotizar y estimar.

Pasados unos dos días, todos terminaron de opinar lo que creían necesario para el proyecto y tenés a tu disposición el [documento que especifica todo lo que se necesita para presupuestar y estimar](/workforce/workflow/flows/website/docs/specified.md) (también en ClickUp).

Con ese documento procedés a obtener un presupuesto (Google Sheets) y estimados de tiempo (Doc de ClickUp), ya tenés todas las herramientas para volver con Andra y decirle cómo sería todo.

|¿Qué haces?|
|:--:|
|[Call con Andra.](./0)|
|[Me hago un tecito.](./1)|
# Call con Andra

Llegó la hora de la verdad, tenés a mano los estimados y el presupuesto, ahora solo queda contarle a Andra y ver qué le parece. Respiras, le pasas un link de Google Meet y que pase lo que tenga que pasar.

`38 minutos después...` (trackeados con [toggl](https://toggl.com))

Presupuesto aceptado! Vamos para adelante con el proyecto. Y ahora qué? No nos prepararon para esto, estamos acostumbrados al rechazo, a que nos digan que no. Bueno, para eso está esta guía, recordá que es una guía, no un cuento.

Primero que nada, creas el proyecto en ClickUp y pasas toda la información ([requirements](/workforce/workflow/flows/website/docs/requirements.md) y [specified](/workforce/workflow/flows/website/docs/specified.md)) a la carpeta del proyecto. Después de eso avisas que el proyecto está confirmado por Discord así el resto de las cabezas se pone de acuerdo en la fecha de comienzo y se ponen a buscar recursos para cubrir las tareas. Ahora solo queda esperar.

`2 días después...`

Qué difícil es esperar no? Pero ya tenés:

* Estimados de milestones de branding.
* Estimados de milestones de diseño del sitio.
* Repositorio de GitLab preparado para trabajar.
  * Issues creados.
  * Entornos configurados.
* Milestones de GitLab alineados con ClickUp.
* Estimados de milestones de marketing.

Este proyecto parece tener entregables cada una semana aproximadamente, así que una vez por semana vas a poder calmar las ansias de Andra y hacerle saber que no desaparecimos.

## Y también tenés... un equipo asignado! 🥳

![team](/workforce/workflow/flows/website/docs/team.jpg)

Estas son las personas que vas a cuidar y proteger con tu vida, recordá que en POW son la prioridad.

|¿Qué haces?|
|:--:|
|[Procrastinar](./0)|
|[Vamos al final de la primera semana de trabajo.](./1)|
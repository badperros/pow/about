# Final de la primera semana de trabajo

Ooooh entregables! Es hora de mostrarle algo al cliente, como las primeras semanas son de branding probablemente haya un par de pruebas y muestras para obtener feedback del cliente.

Ya que el cliente no tiene contacto directo con la persona designada para el diseño, vas a ser super exhaustivo a la hora de documentar el feedpack provisto por este.

Ese feedback vuelve en forma de algo accionable para la persona encargada del branding, así puede seguir trabajando.

Este proceso se va a seguir repitiendo hasta que se cierre toda la etapa de branding, lo mismo va a suceder con el diseño del sitio web. Todo lo que es diseño se va a manejar de esta manera y es muy importante que todo esté validado para poder pasar a la etapa de desarrollo.

|¿Qué haces?|
|:--:|
|[El diseño está todo OK, desarrollemos.](./0)|
|[TO-DO: Problemas en etapa de diseño.](./1)|
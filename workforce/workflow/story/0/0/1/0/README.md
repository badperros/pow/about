# Desarrollemos

Increible la cantidad de idas y vueltas que hay que dar en la etapa de diseño no? Bueno, olvidate de eso por acá, si llegamos acá es porque está todo confirmado y no hay vuelta atrás; si llega a haber cambios ya estamos en el momento en el que se tienen que recotizar.

Qué hay que entregarle al equipo de desarrollo para que trabaje tranquilo?

* Figma con diseños mobile y desktop.
* Aclaraciones que hagan falta de diseño.

`1 semana después...`

Listo el pollo, la Home del sitio está maquetada y lista para probarla.

La vas a abrir, y no la vas a testear así nomás, sino dicen que en su compu no pasaba. La vas a abrir desde Firefox, Chrome y Safari, y si es posible, Edge. Y ahí, lo resizeas todo, testeas el responsive, hacés click en todo, pero no desde un solo dispositivo, porque vas a ser hábil testeador y te comés un garrón de la gran flauta.
Vos estabas en un estado de testeo y QA violento y de locura, medis con un calibre el espacio entre los divs, abrís el sitio desde tu BlackBerry 9300, desde tu heladera smart, para demostrar tu estado de locura y de inconsciencia temporal. Me explico?  
Además tenés que tener una latita de Monster a mano, y si tenés un paquete de gomitas, papoteate. Y levantas issues así, sos inimputable hermano, en dos días los tenés arreglados.

De ese proceso de QA encontramos cosas que no nos convencieron, así que vamos a crear issues en GitLab (porque son súper accionables, y a los programadores les hablamos por ahí) y te vas a crear una tarea de follow up de esos issues en ClickUp. Si ves que el equipo asignado no suele ver los issues, podrías mandar un mensaje al canal de Discord del proyecto para avisarles que hay issues nuevos, pero siempre por medio de los canales y no por medio de comunicaciones directas.

Esto se va a seguir repitiendo durante los próximos milestones, hasta que esté todo integrado y testeado. Y cuando el sitio esté listo para salir a producción solo queda definir en donde hostearlo. Hay un FTP? No tienen hosting? Todo eso lo vamos a aclarar, vamos a pasar los accesos necesarios o vamos a pedir que se tramite un hosting.

Acordate también de los META Tags del sitio, seguramente acá necesites coordinar con la gente de MKT para tenerlos listos.

Listo, fin del desarrollo.

|¿Qué haces?|
|:--:|
|[Desarrollo OK, vamos con el depto de marketing.](./0)|
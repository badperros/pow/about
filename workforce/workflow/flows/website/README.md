# Website Flow

# 0. Requerimientos

|IN|Roles|Canales|
|--|--|--|
|Mail de contacto|Project Manager / Product Owner<br>Cliente|Mail<br>WhatsApp|


Recibiste contacto de un potencial cliente, te contactas, obtenés la información necesaria para poder explicarle el proyecto al resto del equipo y lo bajas a un documento.

|OUT|Roles|Canales|
|--|--|--|
|[Requerimientos](./docs/requirements.md)|Project Manager / Product Owner|ClickUp doc dentro de Incoming|

# 1. Especificación y Estimación

|IN|Roles|Canales|
|--|--|--|
|[Requerimientos](./docs/requirements.md)|Project Manager / Product Owner<br>Design Lead<br>Marketing Lead<br>Tech Lead|ClickUp<br>Discord<br>Meet|

Con la información bajada a algo que todos entendemos, pasamos a especificar lo que creemos necesario. Esto se hace en base al tarifario y "paquetes" de servicios que tenemos.

|OUT|Roles|Canales|
|--|--|--|
|[Especificaciones y Estimados](./docs/specified.md)|Project Manager / Product Owner<br>Design Lead<br>Marketing Lead<br>Tech Lead|ClickUp doc dentro de Incoming|

# 2. Negociación

|IN|Roles|Canales|
|--|--|--|
|[Especificaciones y Estimados](./docs/specified.md)|Project Manager / Product Owner<br>Cliente|Mail<br>WhatsApp<br>Meet<br>Reunión en persona|

Con los estimados que surjan de la etapa anterior ya se puede volver con la persona que contactó originalmente por el proyecto y mostrarle una propuesta de trabajo.

En caso de que se acepte la propuesta de trabajo, se procede a:
* Crear la carpeta del proyecto dentro de Project Management en ClickUp
* Avisar por Discord que se confirmó el proyecto

|OUT|Roles|Canales|
|--|--|--|
|Contrapropuesta / Aceptación|Project Manager / Product Owner|ClickUp<br>Discord|

# 3. Planificación

|IN|Roles|Canales|
|--|--|--|
|[Especificaciones y Estimados](./docs/specified.md)|Project Manager / Product Owner<br>Design Lead<br>Marketing Lead<br>Tech Lead|ClickUp<br>Discord<br>Meet|

Ya con el proyecto confirmado podemos armar la planificación mas fina, esto conllevará encadenar el diseño con el desarrollo y estimar qué se hará en cada semana del proyecto, para poder definir entregables y calmar ansiedades.

Esta planificación tendría que tener en cuenta demoras por parte del cliente en dar feedback, pero debería armarse de manera que el proyecto pueda continuar lo más que pueda.

|OUT|Roles|Canales|
|--|--|--|
|Planificación (Lista y Timeline)|Project Manager / Product Owner<br>Design Lead<br>Marketing Lead<br>Tech Lead|ClickUp|
|Planificación (Issues)|Tech Lead|GitLab|

# 4. Trabajo

Manos a la obra. Cada departamento se va a manejar con su planificación interna intenando mantenerse dentro de las fechas pautadas.

## 4.1 Diseño

TO-DO: Flow de diseño.

### QA

TO-DO: Proceso de QA de diseño

## 4.2 Marketing

TO-DO: Flow de marketing.

### QA

TO-DO: Proceso de QA de marketing

## 4.3 Desarrollo

Manos a la obra gente, lo primero que hacemos es preparar el repositorio:
* Crear entornos.
* Preparar componentes para que las personas encargadas de la maquetación tengan donde dejar su trabajo.

Luego de esto sigue la planificación que está en los issues, se sigue con eso, se ajustan los issues que surjan de la etapa de QA y se avanza hasta llegar a la fecha de entrega.

Una vez finalizado el trabajo se procede a:
* Integrar lo que haya que integrar
* Dominio definitivo
* Deploy a hosting producción

### QA

#### Problemas de desarrollo

Posibles problemas:
* Hago un resize del sitio y se ve roto.
* Hago click en un botón y no pasa nada.
* Hago hover en un botón y no cambia el aspecto del mouse.
* Hago click en enviar formulario mientras se está mandando y se envía 16 veces.
* Explotó la computadora.

Estos problemas son accionables y solucionables, son resultado directo de la etapa de QA y están listos para que cualquiera se ponga a trabajar en ellos.
Para reportarlos, la persona con el rol de Project Manager o QA debería ingresar al repositorio del proyecto en GitLab y crear los issues con las etiquetas y el milestone correspondientes (Milestone de QA, `priority::bug` en etiqueta).
Si la tarea es de funcionalidad, asignarla al dev encargado de funcionalidad. Si la tarea es de maquetación, asignar al que corresponda.

#### Problemas de interacción / usabilidad

Si surgen este tipo de problemas:
* Falta un spinner de carga en el formulario de contacto
* El sitio tarda mucho en cargar y habría que poner un loader
* Hay que mostrar mensajes de error
* Necesito marcar de alguna manera que se llegó al fin del contenido

Estos no son problemas de desarrollo, son problemas de diseño y no son accionables. Por lo que van a ir a ClickUp a la persona encargada del diseño. Cuando crees la tarea recorda adjuntar:

* Captura de cómo se ve.
* Dibujo / sketch de cómo crees que se debería ver.
* Referencias.
* Texto describiendo el problema.

Una vez que esto se soluciona, se puede pasar a crear un issue en GitLab indicando que hay un feature listo para desarrollar, describiendo lo que hace falta hacer y los diseños / textos / lo que sea correspondientes.

#### Cambios de texto / internacionalización

De esto se va a encargar la persona que tenga el rol de Project Manager. La estructura de los sitios web suele ser la misma para todos los desarrollos, así que siguiendo el siguiente instructivo podrían cambiar todos los textos del sitio:

![Web IDE para modificar textos](./docs/edit-text.mp4)


#### Cambios de imágenes

La idea sería no hacer cambios de imágenes hasta la entrega final, para no llenar al equipo de desarrollo de issues irrelevantes. Las imágenes probablemente se puedan cambiar desde el administrador.


# Info del proyecto
Una empresa de tapitas de plástico de lapiceras necesita un sitio web donde mostrar un catálogo de los diseños que se pueden pedir, y un formulario de contacto para pedir un diseño personalizado. Por ahora no necesitan una tienda, pero más adelante tal vez si.

Aparte de esto también quieren que les gestionemos las redes sociales (Mencionaron instagram, pero deberíamos ver cual es la red que mas les conviene?) y hagamos campañas en Google Ads.

Les pregunté si tenían marca, pero me dijeron que el logo lo hizo su sobrino que se da maña con las computadoras, así que necesitarían un rebranding.

# Info de la empresa
Nombre cliente: Andra.  
Cómo nos conocemos: Fuimos juntos a Jardín.  
Teléfono contacto: +54 9 11 XXXX XXXX  
Email: XXXX@XXXX.com

# Qué pienso que hay que hacer?
* Sitio web administrable
  * Home con video institucional.
  * Formulario de contacto para cotización.
  * Catálogo.
    * Los campos que van a tener cada producto son:
      * Campo 1
      * Campo 2
      * Campo 3
* Campaña AdWords
* Plan de redes
* Branding

# Referencias

* http://sitiodetapitas.com
* http://tapitasbizarras.com
* http://mistapitas.com

# Tiempo

No tienen ninguna urgencia

# Info útil

Tienen un sitio actualmente hecho con Wix: https://tapitas69.wixsite.com

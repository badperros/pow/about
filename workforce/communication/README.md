# Comunicación

En POW nos tomamos muy en serio la comunicación y la transparencia, no queremos que estés todo el día intentando buscar información sobre un proyecto o tengas que preguntarle a alguien para que te pase info (o viceversa).

Todos nuestros documentos son colaborativos y abiertos, si sentís que tenés algo para aportar, sos más que bienvenido a sumar tu aporte. Y si falta información, siempre hay lugares para dejar comentarios y pedirla.

## Importante!

A veces es difícil expresarse mediante texto, y tal vez decimos algo que la otra persona interpreta de una manera distinta a la que pretendíamos. Por eso te dejamos estos valores que nosotros creemos pueden ayudar a evitar y tratar con esas situaciones:

*   No hay ego: No defiendas un punto para ganar un argumento o doblar la apuesta sobre un error.
*   Asumir intenciones positivas: Si un mensaje no es lo suficientemente claro o parece negativo, asumí una intención positiva mientras pedís aclaración.
*   Conocé a los demás integrantes. Compenetrarte con lo que le pasa al otro genera confianza.
*   Decí gracias. Si agradecés en cada oportunidad que tenés, creas un clima donde el feedback se vé como un regalo más que como un ataque.
*   Se amable. No cuesta nada ser amable, aún cuando creés que la persona del otro lado no lo merece.
*   Es imposible saberlo todo, no podés saber como se interpreta lo que decís sin preguntarlo.

### Auto servicio y Auto aprendizaje.

Si bien es muy tentador pedir ayuda automáticamente, intentemos buscar información por nuestra cuenta. Ya sea googleando, en foros o leyendo documentación. Probablemente la duda que tengamos ya la haya tenido alguien antes y ya haya sido respondida.

Obviamente a todos nos pasa que no encontramos respuesta a algunos problemas, y tal vez a alguno de nosotros ya nos pasó antes, así que si sentís que descartaste todos tus métodos para buscar información, sentite cómodo de mandar un mensaje a #general o al canal que corresponda para preguntar.

Ah, eso si, una vez que encuentres la solución, dejala documentada para que otra persona no tenga que pasar por lo mismo la próxima!

---

## Planning, Tareas, Discusión, Documentación y Notificaciones de proyectos

Intentamos centralizar toda esa información en [ClickUp](https://clickup.com/?noRedirect=true). Dentro de ClickUp vas a encontrar un Space de Project Management que es donde se va a encontrar toda la info de cada proyecto.

### Planning

Haciendo click en algún proyecto vas a encontrar una lista de tareas junto a un Gantt / Timeline. El timeline nos ayuda a entender las fechas de entrega y el avance del proyecto.

### Tareas

En la home de ClickUp vas a encontrar tu inbox con todas tus tareas asignadas, y sino las podes ver desde la lista de tareas de cada proyecto.

### Discusión

Se te ocurrió una nueva idea para un proyecto? Queres pelotear algo con otra persona o con cualquiera que esté trabajando en ese proyecto? Podes crear un chat adentro de la carpeta del proyecto y etiquetar (o agregar como watcher) a las personas que creas que le pueda interesar.

Si tenés mucho para decir, tal vez te convenga crear un doc adentro del proyecto, y mandar el link por el chat.

### Documentación

ClickUp permite agregar documentos por cada proyecto, dentro de esa pestaña vas a encontrar toda la info necesaria para ponerte al tanto del laburo que se hizo y se planea hacer.

### Notificaciones

Esta debe ser una de las herramientas mas poderosas de ClickUp, es lo que nos permite comunicarnos asincrónicamente. Qué significa esto? Que si necesitas algo de alguien pero sin urgencia, necesitas una aclaración, o queres charlar un tema no crítico podes dejarlo en ClickUp etiquetando a las personas que le competan sin bombardearlas de notificaciones.

Esto nos permite una comunicación un poco mas sana, y hace que a la persona que etiquetes no le lleguen notificaciones fuera de su horario laboral.

## Reuniones

Siempre que haya que reunirnos por algún motivo va a haber una sala de [Google Meet](https://meet.google.com) disponible. Intentamos que las reuniones sean un último recurso, y mientras más podamos dejar asentado en documentación mejor.

Antes de cada reunión la idea es dejar una agenda de los temas a charlar (así todos tenemos claro el motivo y objetivo de la reunión) y al final de la reunión se hace un resumen de lo que se habló para poder poner al tanto a los que no pudieron estar presentes, o hasta para nosotros mismos en un futuro.

## Chat / Mensajería

Estar online no siempre significa que estamos disponibles para que nos hablen, así que intentemos mantener las comunicaciones laborales por chat al mínimo posible. Si tenés ganas de mandar un meme, preguntarle a alguien cómo está, o algo así está perfecto y es más que bienvenido; pero si tenés consultas respecto a un proyecto en curso, intentemos mandarlas por ClickUp o coordinar una reunión (no necesariamente por meet) para hablar con esa persona.

### ¿Por qué no puedo mandar mensajes sobre proyectos en el chat así nomás?

Muchos de nosotros estamos trabajando en más de un proyecto a la vez y tal vez nos reservamos un momento del día para chequearlos, al mandar un mensaje por chat está esa sensación de inmediatez, de la notificación que llega y la necesidad de contestar. 

Nuestra idea es sacar esa ansiedad del día a día.

# Soy un dev y quiero issues

Bueno bueno, ahí va.

El proceso anterior es más para el laburo de planificación, management, marketing, etc. y no está tan ajustado al proceso de trabajo de desarrollo. Por eso todo lo que son tareas de desarrollo, bugs y documentación técnica lo manejamos desde el repositorio.

## Milestones

Los milestones son el punto de encuentro entre ClickUp y GitLab, la idea es que este milestone represente otro milestone en ClickUp. Por ejemplo, si manejamos el proyecto en sprints, vamos a tener un milestone en GitLab por cada semana (o dos) que pasen de proyecto. 

Esto nos permite setear objetivos semanales y darnos una sensación de progreso, así como también nos es más facil mantener sincronizados ClickUp y GitLab.

## Issues

Todas las tareas van a estar representadas por un issue, la idea es que adentro de cada uno tengas toda la información necesaria para llevarlo a cabo. Si es una feature probablemente haya un link a la documentación de la cual salió, si es un bug va a tener capturas e información relevante, y si es una tarea de maquetado va a tener capturas de lo que hay que hacer y algún link al Figma.

Como bien sabemos, los issues se pueden cerrar por medio de los commits y desde los merge requests. Intentemos manejar los cierres desde ahí y no manualmente, así sabemos qué hace cada merge y en qué momento específicamente se cerraron.

## Merge Requests

Por lo general no laburamos directamente en master porque no somos animales, sino que abrimos una rama, hacemos todo nuestro trabajo tranquilos (pusheando de vez en cuando, porque nunca sabés cuándo se te va a caer una taza de café arriba de la compu) y cuando creemos que está todo listo para que alguien lo revise, le asignamos el merge request a esa persona.

Todo el feedback de los cambios que mandes, se van a tratar desde el Merge Request, y una vez que esté todo ok, se mergea.

## Wiki

¿En dónde está hosteado esto? ¿Qué hace cada microservicio? ¿Diagramas?

Todo eso va en la wiki, la idea es que el proyecto lo pueda agarrar cualquier persona sin que alguien tenga que estar todo el tiempo encima para explicarle dónde está cada cosa. Y si alguno de nosotros quiere saber cómo está hecha alguna parte del proyecto en la que no tuvo participación, pueda leer cómo funciona y por qué se tomaron ciertas decisiones (y hasta proponer cambios!)

## Documentación de código

Dale que no cuesta nada. Copiaste algo de StackOverflow? Deja el link.

# Join us

El proceso para formar parte de POW es bastante simple, no le negamos oportunidades a nadie y siempre vas a tener la posibilidad de demostrarnos qué es lo que sabes hacer.

# Cómo es el proceso?

## El swipe

Lo primero que tiene que pasar es conocernos de alguna manera, mandanos un mail, hablanos por instagram o rompe alguna API nuestra en producción a las 3 de la mañana (ahí si te voy a querer conocer).

## El match

Supongo que cuando nos hablaste nos mandaste un CV, portfolio, una carta de presentación que copiaste de Google y alguna que otra cosa más. Alguno de nosotros va a estar chequeando esas cosas, y te vamos a mandar un mail para hacerte saber que todo llegó bien.

Contanos qué es lo que te hizo seguir la carrera en la que estás (o querés estar), cuáles son tus aspiraciones (tenés un emprendimiento? queres viajar?), si tenes hobbies (cocinas? jugas a los jueguitos? consola o pc?), y el nombre de tus mascotas (las plantas cuentan).

## La cita

Una vez que nos conocimos creo que deberíamos dar el siguiente paso y vernos por primera vez, por lo que vamos a coordinar una call por Google Meet junto a parte del equipo para conocerte un poco mejor (si jugás algún jueguito puede ser por Discord y sería como que nos paguen por jugar).

## La prueba de fuego

No hay mejor manera de corroborar todo lo que charlamos que con un proyecto, así que en cuanto tengamos algo que coincida con tus skills y sintamos que es un buen primer proyecto te vamos a avisar.

### Cuál sería un buen primer proyecto?

Mmmmm, eso va a depender del puesto y de lo que tengamos en stock en ese momento. Pero algunos ejemplos serían:

* Maquetador: Maquetación de sitio web institucional chiquito.
* Frontend dev: Integración de un sitio web con alguna API y un poquito de lógica de negocio.
* Backend dev: Implementación de algun feature / issue chiquito en algún sistema no crítico.
* Community Manager: Planificación y estrategia de redes sociales para alguna marca.
* Fotógrafo / Editor: Sacar fotos producto y editarlas.
* Diseñador UX: Diseñar una experiencia de usuario para alguna mini app que nos pidan.
* Diseñador UI: Diseñar un sitio web institucional.
* Diseñador editorial: Desarrollo de piezas gráficas.

## Welcome

Si todo salió bien y todos nos sentimos cómodos laburando juntos, ya está, no necesitamos nada más que eso para considerarte parte. Durante todo este proceso fuimos charlando cómo sería nuestra relación laboral y a medida que vayan surgiendo proyectos te vamos a ir llamando.

Nuestra idea es darnos feedback constántemente, si detectas oportunidades de mejora, no sentís que el proyecto se haya manageado bien o el cliente te mandaba mails todos los días y se ponía re gede (intentamos que esto no pase eh), avisanos y a nosotros nos re sirve para ver qué podemos hacer para mejorar.
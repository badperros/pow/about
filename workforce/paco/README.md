  

# Plan de Acción de Cuarentena y Oficina


COVID-19 hizo que tengamos que repensar muchas cosas, de darnos cuenta de lo poco higiénico que es nuestro día a día y de cómo las prácticas del trabajo cotidiano no escalan. En base a cómo van reaccionando empresas de afuera (que ya pasaron el invierno y el pico de contagios) y las decisiones que se van tomando en el país deberíamos tomar una decisión de cómo seguir en el próximo semestre.

  

## Cómo reaccionan las empresas de afuera


*   [https://www.nytimes.com/2020/05/21/technology/facebook-remote-work-coronavirus.html](https://www.nytimes.com/2020/05/21/technology/facebook-remote-work-coronavirus.html)
*   [https://www.forbes.com/sites/zackfriedman/2020/05/08/google-facebook-amazon-work-home/#26f98b81eaea](https://www.forbes.com/sites/zackfriedman/2020/05/08/google-facebook-amazon-work-home/#26f98b81eaea)
*   [https://observer.com/2020/05/coronavirus-work-from-home-facebook-google-microsoft-apple-amazon/](https://observer.com/2020/05/coronavirus-work-from-home-facebook-google-microsoft-apple-amazon/)
*   [https://www.axios.com/tech-work-from-home-remote-coronavirus-83658822-1cd8-4fbf-91f6-a2ee14401730.html](https://www.axios.com/tech-work-from-home-remote-coronavirus-83658822-1cd8-4fbf-91f6-a2ee14401730.html)

  

## Cómo esto afecta los espacios de Coworking


*   [https://www.globest.com/2020/05/08/researchers-predict-uptick-for-private-office-spaces-falloff-in-coworking-after-covid-19/?slreturn=20200423181943](https://www.globest.com/2020/05/08/researchers-predict-uptick-for-private-office-spaces-falloff-in-coworking-after-covid-19/?slreturn=20200423181943)
*   [https://magazine.realtor/daily-news/2020/04/21/covid-19-prompts-rethinking-of-coworking-spaces](https://magazine.realtor/daily-news/2020/04/21/covid-19-prompts-rethinking-of-coworking-spaces)
*   [https://www.5280.com/2020/04/how-colorado-coworking-spaces-are-adjusting-during-covid-19/](https://www.5280.com/2020/04/how-colorado-coworking-spaces-are-adjusting-during-covid-19/)

  

## Cómo reaccionamos como país

*   [https://www.infobae.com/politica/2020/05/19/coronavirus-en-la-argentina-alberto-fernandez-analiza-extender-la-cuarentena-obligatoria-hasta-el-8-de-junio/](https://www.infobae.com/politica/2020/05/19/coronavirus-en-la-argentina-alberto-fernandez-analiza-extender-la-cuarentena-obligatoria-hasta-el-8-de-junio/)
*   [https://www.infobae.com/politica/2020/05/21/comenzo-una-reunion-clave-en-casa-rosada-para-definir-protocolos-para-el-transporte-durante-la-proxima-etapa-de-la-cuarentena-y-analizar-la-situacion-de-los-asentamientos-del-amba/](https://www.infobae.com/politica/2020/05/21/comenzo-una-reunion-clave-en-casa-rosada-para-definir-protocolos-para-el-transporte-durante-la-proxima-etapa-de-la-cuarentena-y-analizar-la-situacion-de-los-asentamientos-del-amba/)

  

## Cómo viene la cosa

*   [https://www.elterritorio.com.ar/estiman-que-la-curva-de-contagios-creceria-hasta-el-15-de-junio-69484-et](https://www.elterritorio.com.ar/estiman-que-la-curva-de-contagios-creceria-hasta-el-15-de-junio-69484-et)

  

Creo que deberíamos considerar el trabajo remoto al menos por el siguiente semestre, hasta que pase el frío y llegue el verano.

  

* * *

  

# Pain points

  

## Coworking

*   Tendrían que hacer mucho énfasis en la limpieza, cosa que nunca hicieron y si quisieran hacerlo probablemente no les den los números.
*   Alto tráfico de gente, también considerando que no solo son coworkers los que van al galpón.

  

## Transporte

*   Suspensión del transporte público debido a que no escala y no está preparado para funcionar de esta manera (el transporte público es insanitario y con menos gente pasa a ser muchísimo menos rentable). Ivi, por ejemplo, depende del transporte público; y yo también los días que llueve.

  

## Contacto

*   Deberíamos fomentar las reuniones no presenciales con clientes, para exponernos lo menos posible.

  

## Sociedad

*   Cuarentena por tiempo indefinido.
*   Si se reabre en invierno, va a ser desastroso y van a tener que cerrarlo de nuevo.

  

* * *

  

# Evaluación

  

## Situación actual

### Oficina

Tenemos la oficina del galpón con hardware y cosas que quedaron tiradas allá. Por suerte pudimos ir y llevarnos las cosas que necesitábamos para nuestro laburo del día a día.

  

### Proyectos

Por suerte el ingreso de proyectos no disminuyó, y es más, hasta aumentó. Tenemos más proyectos que los que podemos agarrar con la fuerza laboral con la que contamos.

  

### Gente

Cada uno está laburando desde su casa, con su computadora personal, su propia conexión a internet y sus propios recursos.

  

## Ventajas

*   Abono reducido del galpón (Santi voy a necesitar que me aclares cómo lo estamos manejando).
*   Nuestro laburo se puede hacer de manera remota sin problemas.

  

## Desventajas

*   Más desafíos en lo que respecta a comunicación.
*   Falta el factor humano, el contacto del día a día, compartir un mate.
*   Pasamos de un horario fijo de oficina dónde siempre nos podemos encontrar laburando a uno full remote. Tal vez antes nos podíamos sacar dudas en el momento y ahora no.
*   Cada uno tiene que usar su computadora personal y surgen problemas como:
    *   Falta de espacio disponible.
    *   Procesadores mas lentos.
    *   Escritorios no ergonómicos.
    *   Velocidad / estabilidad de internet no garantizada.

  

## Posibles soluciones

*   Usar lo que nos ahorremos del galpón en pagar abonos de internet de empleados. (Se podrá hablar con alguna empresa de telco para descuentos?)
*   Proveer una Raspberry Pi / computadora a los empleados para que podamos garantizar un mínimo.
*   Proveer hardware adicional necesario (monitores, teléfonos, adaptadores, discos).
*   Asegurarnos que todos tengan un espacio de trabajo ergonómico.
*   Proveer acceso a remoto a máquinas para testeo.
*   Fomentar prácticas de seguridad (2FA y esas cosas).
*   Cuando se vaya levantando la cuarentena, hacer una reunión semanal presencial, para mantener el contacto.

# Manifesto que nos choreamos

* We always answer emails to each other within a day, even if it’s just to say ‘got it’.
* We’ll pay invoices before they’re due.
* We don’t use voice audios to express ourselves, your phone has a "call button" for sure.
* We’ll never intentionally trick anybody with loopholes in our agreements or oversell something. Instead, we’ll be as clear as we can and honor what we said, and expect that you’ll do the same.
* We always assume our responsibility and mistakes.
* If we’re not sure, we’ll ask each other.
* We don’t miss deadlines.
* We don’t sprint at the end, we sprint at the beginning.
* We eagerly highlight the potholes ahead, but spend no time casting blame after something fails.
* We are intentional and specific about the work. “Who is it for” and “what is it for?” are the two key design questions.
* If it’s not working, we’ll say so, and do it with specificity and kindness.
* We’ll pay a lot but expect to get more than we paid for.

# Camino a la eficiencia

1.  Nos comunicaremos a veces en tiempo real, asincrónicamente la mayor parte del tiempo.
2.  Las reuniones son un último recurso, no la primera opción.
3.  Hablar sólo ayuda a quienes estén en la habitación, escribir ayuda a todos. Gente que no pudo asistir, futuras personas que se podrían unir al equipo o incluso alguien revisando dudas más adelante.
4.  No esperar o pedir revolución inmediata a no ser que sea una emergencia. Esperar que todo sea de inmediato resulta tóxico.
5.  Mala comunicación resulta en más trabajo.
6.  "Ahora" suele ser un mal momento para decir lo que se te acaba de ocurrir. Es mejor dejar filtrar la idea con el tiempo. Lo que termine quedando en tu cabeza seguro vale la pena decirlo.
7.  El cierre del día, tiende a convencerte de que hiciste un buen trabajo, pero la mañana siguiente tiene una forma de decirte la verdad. Si no estás seguro de algo, descansá y mañana lo confirmás.
8.  Si hay algo difícil de escuchar o compartir, intentá hacer preguntas luego de decirlo. Terminar una conversación sin invitación a seguirla tiende a silencio grupal pero conjetura personal, es ahí donde se generan los rumores y las fricciones.
9.  Escribí a la hora que corresponde, compartir algo a las 5am podría mantener a alguien trabajando más de la cuenta. Quizás tengas tiempo el sábado a la noche para mandar algo, pero mandándolo en ése momento podría poner a trabajar a las personas durante su descanso. Mandarlo el lunes a la mañana quizás se empaste con otras comunicaciones.. Seguramente no haya un tiempo ideal, pero seguro hay un tiempo erróneo. Mantenelo presente antes de apretar _Send._
10.  El tiempo está de tu lado, apurarse hace mal a las conversaciones.
11.  Siempre preguntá si lo que dijiste quedó claro. Preguntá si olvidaste algo. Preguntá si alguien estaba esperando algo que no cubriste, resolvé las dudas antes de que se agranden con el tiempo.

# POW

En este repositorio vas a encontrar toda la información que necesitas para laburar con nosotros, o enterarte cómo laburamos.

# Qué hacemos en POW?

En POW tenemos 3 verticales principales.

## Production

Esta unidad de negocio se encarga de producción de eventos, cosas a gran escala que requieren recursos humanos u organización de cosas. Puede ser desde un festival de música a un evento empresarial.

### Quiénes se encargan de esto?
| | |
| :---: | :---: |
| <img src="https://www.pow.lat/images/uploads/whatsapp-image-2019-08-09-at-15.19.25_sqare.png" height="120" width="120"><br>**Santi Maya**<br>General Manager | <img src="https://www.pow.lat/images/uploads/edu.jpeg" height="120" width="120"><br>**Edu Buery**<br>Producer |


## [Operations](/operations)

Todos los proyectos de agencia / estudio de diseño / desarrollos simples vienen por este lado.

### Quiénes se encargan de esto?

| | | | |
| :---: | :---: | :---: | :---: |
| <img src="https://www.pow.lat/images/uploads/jbs.jpeg" height="120" width="120"><br>**Juan Bautista Sánchez**<br>Project Manager | <img src="https://www.pow.lat/images/uploads/0.jpg" height="120" width="120"><br>**Ivi Benozzi**<br>Marketing Analyst | <img src="https://www.pow.lat/images/uploads/whatsapp-image-2019-08-09-at-15.19.25_sqare.png" height="120" width="120"><br>**Santi Maya**<br>General Manager | <img src="https://www.pow.lat/images/uploads/tommygorkin.jpg" height="120" width="120"><br>**Tomas Gorkin**<br>General Manager |


## [Workforce](/workforce)

Esta vertical es mas simbólica, nos da una identidad de equipo de trabajo y nos agrupa a todos.

| | | | |
| :---: | :---: | :---: | :---: |
| <img src="https://www.pow.lat/images/uploads/0.jpg" height="120" width="120"><br>**Ivi Benozzi**<br>Marketing Analyst | <img src="https://www.pow.lat/images/uploads/0-2.jpg" height="120" width="120"><br>**Mati Torelli**<br>Front-end Dev | <img src="https://www.pow.lat/images/uploads/jbs.jpeg" height="120" width="120"><br>**Juan Bautista Sánchez**<br>Project Manager | <img src="https://www.pow.lat/images/uploads/0-1.jpg" height="120" width="120"><br>**Gonzalo Ulla**<br>UX / UI Designer |
| <img src="https://www.pow.lat/images/uploads/edu.jpeg" height="120" width="120"><br>**Edu Buery**<br>Producer | <img src="https://www.pow.lat/images/uploads/whatsapp-image-2019-08-09-at-15.19.25_sqare.png" height="120" width="120"><br>**Santi Maya**<br>General Manager | <img src="https://www.pow.lat/images/uploads/tommygorkin.jpg" height="120" width="120"><br>**Tomas Gorkin**<br>General Manager |


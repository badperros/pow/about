# Operations

Este departamento se encarga de todo el laburo tradicional de pequeña y mediana escala, hicimos cosas copadas como:

* [Beldent - El ultimo no se entrega](https://www.pow.lat/portfolio/beldent/)
* [Tang - Batalla de rap](https://www.pow.lat/portfolio/tang/)
* [Ayudín - Casi un monumento](https://www.pow.lat/portfolio/ayudin/)

# Qué abarcamos?

## [Marketing](/operations/marketing)

Analizamos, planificamos y ejecutamos campañas de marketing digital.

## [Diseño](/operations/design)

Vamos desde el diseño editorial tradicional hasta el diseño digital de experiencias de usuario.

## [Desarrollo](/operations/development)

Hacemos cosas copadas con tecnologías copadas.
